There are other ways you can use VPN, but you need to pay them directly.

1. [Qianzhen](https://www.wx2021.com/): 10 or 15 yuan / month. Lots of nods. Yet I found it is unstable, the official website would disapear suddently.
2. [v2net](http://v2sub.com/): A little bit more expensive than Qianzhen. 14 yuan / month, 10 GB
3. [avast](https://www.avast.com/zh-cn/download-thank-you.php?product=SLN&locale=zh-cn): Probably could work.
4. [vpnhub](https://vpnhub.updatestar.com/): Doesn't work.
5. [qickq](https://www.quickq.run/#introduce): Expensive. 30 yuan / month.

[Here](https://github.com/xbblog95/v2sub) is how to subscript in linux. To use it, you need to install v2ray in your computer. The network in China sometimes cannot download v2ray properly, one needs to download it in [here](https://www.v2ray.com/chapter_00/install.html), and move all the required file to the target places manually.

Do not use transparent proxy if you still wanna use network without proxy.

