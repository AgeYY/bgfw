# Intro

There are three ways of going across the great firewall.

1. ss: worse than ssr

2. ssr: is faster must risky

3. v2ray: slower, harder to build, but more stable

# Preparation
1. Log in to the server
  1. ssh root@45.32.122.125
  2. open the online _consol_ in vultr
2. Log out to the server
  1. ssh-keygen -R 45.32.122.125


# How to build SS/SSR

1. Buy a VPS

2. Log in to the server (see preparation section)

3. Clone
``` bash
git clone -b master https://github.com/flyzy2005/ss-fly
```

4. Run this command to install ss
```bash
ss-fly/ss-fly.sh
```

And this one to install ssr
```bash
ss-fly/ss-fly.sh -ssr
```

some usefule commands

```bash
启动：/etc/init.d/shadowsocks start
停止：/etc/init.d/shadowsocks stop
重启：/etc/init.d/shadowsocks restart
状态：/etc/init.d/shadowsocks status
 
配置文件路径：/etc/shadowsocks.json
日志文件路径：/var/log/shadowsocks.log
代码安装目录：/usr/local/shadowsocks
```

4. Install the local ssr gui for linux, see _./ssr/sshgui/_

# How to build V2Ray

1. Install _v2ray_ on the server though
```bash
bash <(curl -L -s https://install.direct/go.sh)
```

3. Edit _/etc/v2ray/config.json_ according to [here](https://www.flyzy2005.com/go/go.php?url=https://intmainreturn0.com/v2ray-config-gen/)

if you cannot open this page, try to use vpn or add this line in your hosts

```
104.27.133.214 www.flyzy2005.com
```

4. Install local v2ray though from ./v2ray/GUI
